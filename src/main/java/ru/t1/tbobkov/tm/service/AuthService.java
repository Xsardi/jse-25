package ru.t1.tbobkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.service.IAuthService;
import ru.t1.tbobkov.tm.api.service.IPropertyService;
import ru.t1.tbobkov.tm.api.service.IUserService;
import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.exception.field.LoginEmptyException;
import ru.t1.tbobkov.tm.exception.field.PasswordEmptyException;
import ru.t1.tbobkov.tm.exception.system.PermissionException;
import ru.t1.tbobkov.tm.exception.user.AccessDeniedException;
import ru.t1.tbobkov.tm.exception.user.IncorrectLoginDataException;
import ru.t1.tbobkov.tm.exception.user.UserNotFoundException;
import ru.t1.tbobkov.tm.model.User;
import ru.t1.tbobkov.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IPropertyService propertyService, @NotNull final IUserService userService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User registry(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@NotNull String login, @NotNull String password) {
        if (login.isEmpty()) throw new LoginEmptyException();
        if (password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginDataException();
        final boolean locked = user.getLocked();
        if (locked) throw new IncorrectLoginDataException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash != null && !hash.equals(user.getPasswordHash())) throw new IncorrectLoginDataException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (userId == null) throw new AccessDeniedException();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public void checkRoles(@Nullable Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }
}
