package ru.t1.tbobkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

}
