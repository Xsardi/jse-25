package ru.t1.tbobkov.tm.exception.user;

public class EmailExistsException extends AbstractUserException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }
}
